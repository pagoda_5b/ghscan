import sbtassembly.AssemblyPlugin.defaultUniversalScript

/* Versions */
val CatsV = "2.1.1"
val CatsEffectV = "2.1.3"
val Fs2V = "2.4.2"
val Http4sV = "0.21.5"
val CirceV = "0.13.0"
val CirisV = "1.1.1"
val ScalatestV = "3.2.0"
val ScalaTestScalaCheckV = "3.1.2.0"
val Log4CatsV = "1.1.1"
val LogbackV = "1.2.3"
val QuillV = "3.5.2"
val DoobieV = "0.9.0"

/* Project-wise */
ThisBuild / scalaVersion := "2.13.2"
ThisBuild / organization := "tech.pagoda5b"

/* Root project */
lazy val ghscan = (project in file("."))
  .settings(
    name := "gh-scan",
    version := "0.0.1-SNAPSHOT",
    mainClass := Some("tech.pagoda5b.ghscan.Main"),
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-core" % CatsV,
      "org.typelevel" %% "cats-effect" % CatsEffectV,
      "co.fs2" %% "fs2-core" % Fs2V,
      "org.http4s" %% "http4s-blaze-server" % Http4sV,
      "org.http4s" %% "http4s-blaze-client" % Http4sV,
      "org.http4s" %% "http4s-circe" % Http4sV,
      "org.http4s" %% "http4s-dsl" % Http4sV,
      "io.circe" %% "circe-generic" % CirceV,
      "is.cir" %% "ciris" % CirisV,
      "is.cir" %% "ciris-circe" % CirisV,
      "ch.qos.logback" % "logback-classic" % LogbackV,
      // "io.chrisdavenport" %% "log4cats-core"    % Log4CatsV,  // Only if you want to Support Any Backend
      "io.chrisdavenport" %% "log4cats-slf4j" % Log4CatsV, // Direct Slf4j Support - Recommended
      "org.tpolecat" %% "doobie-core" % DoobieV,
      "org.tpolecat" %% "doobie-postgres" % DoobieV,
      "org.tpolecat" %% "doobie-hikari" % DoobieV,
      "org.tpolecat" %% "doobie-quill" % DoobieV,
      "io.getquill" %% "quill-core" % QuillV,
      "io.getquill" %% "quill-codegen-jdbc" % QuillV,
      "org.scalactic" %% "scalactic" % ScalatestV,
      "org.scalatestplus" %% "scalacheck-1-14" % ScalaTestScalaCheckV,
      "org.scalatest" %% "scalatest" % ScalatestV % "test"
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
    assembly / test := {},
    assembly / mainClass := (run / mainClass).value,
    assembly / assemblyJarName := "ghscan.jar",
    assembly / assemblyOutputPath := baseDirectory.value / "uberjar" / (assembly / assemblyJarName).value,
    assembly / assemblyOption := (assembly / assemblyOption).value
      .copy(prependShellScript = Some(defaultUniversalScript(shebang = true)))
  )

scalacOptions ++= Seq(
  "-target:11",
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-encoding",
  "utf-8", // Specify character encoding used by source files.
  "-explaintypes", // Explain type errors in more detail.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-language:existentials", // Existential types (besides wildcard types) can be written and inferred
  "-language:experimental.macros", // Allow macro definition (besides implementation and application)
  "-language:higherKinds", // Allow higher-kinded types
  "-language:implicitConversions", // Allow definition of implicit functions called views
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-Xcheckinit", // Wrap field accessors to throw an exception on uninitialized access.
  "-Xlint:constant", // Evaluation of a constant arithmetic expression results in an error.
  "-Xlint:delayedinit-select", // Selecting member of DelayedInit.
  "-Xlint:doc-detached", // A Scaladoc comment appears to be detached from its element.
  "-Xlint:inaccessible", // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any", // Warn when a type argument is inferred to be `Any`.
  "-Xlint:missing-interpolator", // A string literal appears to be missing an interpolator id.
  "-Xlint:nullary-override", // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Xlint:nullary-unit", // Warn when nullary methods return Unit.
  "-Xlint:option-implicit", // Option.apply used implicit view.
  "-Xlint:package-object-classes", // Class or object defined in package object.
  "-Xlint:poly-implicit-overload", // Parameterized overloaded implicit methods are not visible as view bounds.
  "-Xlint:private-shadow", // A private field (or class parameter) shadows a superclass field.
  "-Xlint:stars-align", // Pattern sequence wildcard must align with sequence component.
  "-Xlint:type-parameter-shadow", // A local type parameter shadows a type already in scope.
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-extra-implicit", // Warn when more than one implicit parameter section is defined.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals", // Warn if a local definition is unused.
  "-Ywarn-unused:params", // Warn if a value parameter is unused.
  "-Ywarn-unused:privates", // Warn if a private member is unused.
  "-Ywarn-value-discard" // Warn when non-Unit expression results are unused.
)
