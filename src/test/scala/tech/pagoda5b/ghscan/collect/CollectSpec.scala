package tech.pagoda5b.ghscan.collect

import org.scalatest.propspec.AnyPropSpec
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import org.scalacheck.{Arbitrary, Gen}
import cats.implicits._
import cats.effect.IO
import cats.effect.concurrent.MVar
import fs2._

class CollectSpec
    extends AnyPropSpec
    with ScalaCheckDrivenPropertyChecks
    with Matchers {

  import Fixtures._
  import scala.concurrent.ExecutionContext.Implicits.global

  property(
    "The collection process should not muddle with the data passed to it"
  ) {
    implicit val shift = IO.contextShift(global)
    /* provides an adapter which does nothing more but return the input */
    implicit val identityAdapte: DataAdapter[IO, Int, Int] =
      (in: Int) => Stream(in).covary[IO]

    forAll { (ints: List[Int]) =>
      /* given building blocks that does not touch the data from in to out,
       * the composite process should simply pass through the data as-is
       */

      val storeProcess = for {
        out <- MVar.empty[IO, List[Int]]
        listStore = listSink(out)
        _ <- CollectionProcess.run(
          source = listSource(ints),
          store = listStore
        )
        stored <- out.read
      } yield stored

      storeProcess.unsafeRunSync() shouldBe ints

    }
  }

  property(
    "The collection process should apply the filter to data and convert it based on the adapter"
  ) {
    implicit val shift = IO.contextShift(global)
    //let's keep clean as a string [sorry for the bad joke]
    implicit val shower: DataAdapter[IO, Int, String] =
      (in: Int) => Stream(in.show).covary[IO]

    forAll { (ints: IndexList[Int]) =>
      val IndexList(items, dropped) = ints

      //convert to strings, drops some and check it all works out
      val storeProcess = for {
        out <- MVar.empty[IO, List[String]]
        listStore = listSink(out)
        _ <- CollectionProcess.run(
          source = droppingListSource(items),
          store = listStore,
          collectAfter = dropped.some
        )
        stored <- out.read
      } yield stored

      storeProcess.unsafeRunSync() shouldBe items.drop(dropped).map(_.show)

    }
  }

  /** Collects data needed to build the test */
  private object Fixtures {

    /* builds a source from an item list */
    def listSource[A](items: List[A]) = new CollectSource[A] {
      type LastCollected = Nothing

      override def collectFrom(lastCollected: Option[Nothing]): Stream[IO, A] =
        Stream.fromIterator[IO](items.iterator)

    }

    /* builds a source from an item list, allowing to pass the number of initial elements to drop */
    def droppingListSource(items: List[Int]) = new CollectSource[Int] {
      /* how many elements to drop from the list */
      type LastCollected = Int

      override def collectFrom(lastCollected: Option[Int]): Stream[IO, Int] =
        Stream.fromIterator[IO](
          items
            .drop(lastCollected.getOrElse(0))
            .iterator
        )
    }

    /* collects the data storing it into an [[MVar]] as a List */
    def listSink[A](outList: MVar[IO, List[A]]): CollectStorage[A, Unit] =
      (data: Stream[IO, A]) => Stream.eval(data.compile.toList >>= outList.put)

    /* a list with an index, possibly pointing to some element */
    case class IndexList[A](items: List[A], index: Int)

    /* we generate a list and an index internal to the list */
    implicit def listWithValidIndex[A](
        implicit ev: Arbitrary[A]
    ): Arbitrary[IndexList[A]] = Arbitrary(
      for {
        as <- Gen.listOf[A](ev.arbitrary)
        i <- Gen.chooseNum(0, math.max(as.size - 1, 0))
      } yield IndexList(as, i)
    )
  }

}
