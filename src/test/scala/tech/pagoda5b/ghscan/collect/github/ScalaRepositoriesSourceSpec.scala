package tech.pagoda5b.ghscan.collect.github

import tech.pagoda5b.ghscan.collect.github.ScalaRepositoriesSource
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.should.Matchers
import org.http4s._
import org.http4s.client.Client
import cats.implicits._
import cats.effect.IO
import fs2._

class ScalaRepositoriesSourceSpec extends AnyWordSpec with Matchers {

  import Fixtures._
  import scala.concurrent.ExecutionContext.Implicits.global

  "The source collector for scala repositories" should {
    "call the api endpoint with valid request" in {

      implicit val shift = IO.contextShift(global)

      val verificationClient = client(
        acceptIf = request =>
          request.method == Method.GET &&
            request.serverAddr == "api.github.com" &&
            request.uri.path == "/search/repositories" &&
            request.params
              .get("q")
              .exists(q => q.split(' ').forall(expectedQueryField)),
        withResponse = searchResultsJson
      )

      val sut = ScalaRepositoriesSource(verificationClient)

      val output = sut.collectFrom().compile.toList.unsafeRunSync()

      output shouldBe List(searchResultsModel)
    }
  }

  /** Collects data needed to build the test */
  private object Fixtures {
    import java.time.ZonedDateTime
    import java.time.format.DateTimeFormatter.ISO_DATE_TIME
    import RepositoriesModel.{SearchResults, RepositoryItem}

    /** a test client that will respond based on expectations from the request */
    def client(acceptIf: Request[IO] => Boolean, withResponse: String) =
      Client.fromHttpApp[IO](
        HttpApp { req =>
          val resp =
            if (acceptIf(req))
              Response[IO](
                status = Status.Ok,
                body = Stream(withResponse).through(text.utf8Encode)
              )
            else {
              debug(req)
              Response[IO](
                status = Status.BadRequest
              )
            }
          resp.pure[IO]
        }
      )

    /* list of expected query elements */
    val expectedQueryField = Set("language:scala", "mirror:false")

    private def debug(request: Request[IO]) = {
      println(s"method: ${request.method}")
      println(s"server: ${request.serverAddr}")
      println(s"uri path: ${request.uri.path}")
      println(s"params: ${request.params}")
    }

    lazy val searchResultsJson =
      s"""
         |{
         |    "incomplete_results": false,
         |    "items": [
         |         $apacheRepoJson
         |    ]
         |}
        """.stripMargin

    lazy val apacheRepoJson =
      """
         |{
         |  "archive_url": "https://api.github.com/repos/apache/spark/{archive_format}{/ref}",
         |  "archived": false,
         |  "assignees_url": "https://api.github.com/repos/apache/spark/assignees{/user}",
         |  "blobs_url": "https://api.github.com/repos/apache/spark/git/blobs{/sha}",
         |  "branches_url": "https://api.github.com/repos/apache/spark/branches{/branch}",
         |  "clone_url": "https://github.com/apache/spark.git",
         |  "collaborators_url": "https://api.github.com/repos/apache/spark/collaborators{/collaborator}",
         |  "comments_url": "https://api.github.com/repos/apache/spark/comments{/number}",
         |  "commits_url": "https://api.github.com/repos/apache/spark/commits{/sha}",
         |  "compare_url": "https://api.github.com/repos/apache/spark/compare/{base}...{head}",
         |  "contents_url": "https://api.github.com/repos/apache/spark/contents/{+path}",
         |  "contributors_url": "https://api.github.com/repos/apache/spark/contributors",
         |  "created_at": "2014-02-25T08:00:08Z",
         |  "default_branch": "master",
         |  "deployments_url": "https://api.github.com/repos/apache/spark/deployments",
         |  "description": "Apache Spark - A unified analytics engine for large-scale data processing",
         |  "disabled": false,
         |  "downloads_url": "https://api.github.com/repos/apache/spark/downloads",
         |  "events_url": "https://api.github.com/repos/apache/spark/events",
         |  "fork": false,
         |  "forks": 22024,
         |  "forks_count": 22024,
         |  "forks_url": "https://api.github.com/repos/apache/spark/forks",
         |  "full_name": "apache/spark",
         |  "git_commits_url": "https://api.github.com/repos/apache/spark/git/commits{/sha}",
         |  "git_refs_url": "https://api.github.com/repos/apache/spark/git/refs{/sha}",
         |  "git_tags_url": "https://api.github.com/repos/apache/spark/git/tags{/sha}",
         |  "git_url": "git://github.com/apache/spark.git",
         |  "has_downloads": true,
         |  "has_issues": false,
         |  "has_pages": false,
         |  "has_projects": true,
         |  "has_wiki": false,
         |  "homepage": "https://spark.apache.org/",
         |  "hooks_url": "https://api.github.com/repos/apache/spark/hooks",
         |  "html_url": "https://github.com/apache/spark",
         |  "id": 17165658,
         |  "issue_comment_url": "https://api.github.com/repos/apache/spark/issues/comments{/number}",
         |  "issue_events_url": "https://api.github.com/repos/apache/spark/issues/events{/number}",
         |  "issues_url": "https://api.github.com/repos/apache/spark/issues{/number}",
         |  "keys_url": "https://api.github.com/repos/apache/spark/keys{/key_id}",
         |  "labels_url": "https://api.github.com/repos/apache/spark/labels{/name}",
         |  "language": "Scala",
         |  "languages_url": "https://api.github.com/repos/apache/spark/languages",
         |  "license": {
         |      "key": "apache-2.0",
         |      "name": "Apache License 2.0",
         |      "node_id": "MDc6TGljZW5zZTI=",
         |      "spdx_id": "Apache-2.0",
         |      "url": "https://api.github.com/licenses/apache-2.0"
         |  },
         |  "merges_url": "https://api.github.com/repos/apache/spark/merges",
         |  "milestones_url": "https://api.github.com/repos/apache/spark/milestones{/number}",
         |  "mirror_url": null,
         |  "name": "spark",
         |  "node_id": "MDEwOlJlcG9zaXRvcnkxNzE2NTY1OA==",
         |  "notifications_url": "https://api.github.com/repos/apache/spark/notifications{?since,all,participating}",
         |  "open_issues": 206,
         |  "open_issues_count": 206,
         |  "owner": {
         |      "avatar_url": "https://avatars0.githubusercontent.com/u/47359?v=4",
         |      "events_url": "https://api.github.com/users/apache/events{/privacy}",
         |      "followers_url": "https://api.github.com/users/apache/followers",
         |      "following_url": "https://api.github.com/users/apache/following{/other_user}",
         |      "gists_url": "https://api.github.com/users/apache/gists{/gist_id}",
         |      "gravatar_id": "",
         |      "html_url": "https://github.com/apache",
         |      "id": 47359,
         |      "login": "apache",
         |      "node_id": "MDEyOk9yZ2FuaXphdGlvbjQ3MzU5",
         |      "organizations_url": "https://api.github.com/users/apache/orgs",
         |      "received_events_url": "https://api.github.com/users/apache/received_events",
         |      "repos_url": "https://api.github.com/users/apache/repos",
         |      "site_admin": false,
         |      "starred_url": "https://api.github.com/users/apache/starred{/owner}{/repo}",
         |      "subscriptions_url": "https://api.github.com/users/apache/subscriptions",
         |      "type": "Organization",
         |      "url": "https://api.github.com/users/apache"
         |  },
         |  "private": false,
         |  "pulls_url": "https://api.github.com/repos/apache/spark/pulls{/number}",
         |  "pushed_at": "2020-06-27T16:09:37Z",
         |  "releases_url": "https://api.github.com/repos/apache/spark/releases{/id}",
         |  "score": 1.0,
         |  "size": 333250,
         |  "ssh_url": "git@github.com:apache/spark.git",
         |  "stargazers_count": 26544,
         |  "stargazers_url": "https://api.github.com/repos/apache/spark/stargazers",
         |  "statuses_url": "https://api.github.com/repos/apache/spark/statuses/{sha}",
         |  "subscribers_url": "https://api.github.com/repos/apache/spark/subscribers",
         |  "subscription_url": "https://api.github.com/repos/apache/spark/subscription",
         |  "svn_url": "https://github.com/apache/spark",
         |  "tags_url": "https://api.github.com/repos/apache/spark/tags",
         |  "teams_url": "https://api.github.com/repos/apache/spark/teams",
         |  "trees_url": "https://api.github.com/repos/apache/spark/git/trees{/sha}",
         |  "updated_at": "2020-06-27T15:25:49Z",
         |  "url": "https://api.github.com/repos/apache/spark",
         |  "watchers": 26544,
         |  "watchers_count": 26544
       |}
    """.stripMargin

    lazy val apacheRepoModel = RepositoryItem(
      id = 17165658,
      created_at =
        ZonedDateTime.from(ISO_DATE_TIME.parse("2014-02-25T08:00:08Z")),
      default_branch = "master",
      description =
        "Apache Spark - A unified analytics engine for large-scale data processing".some,
      forks = 22024L,
      full_name = "apache/spark",
      git_url = "git://github.com/apache/spark.git",
      homepage = "https://spark.apache.org/".some,
      name = "spark",
      open_issues = 206,
      pushed_at =
        ZonedDateTime.from(ISO_DATE_TIME.parse("2020-06-27T16:09:37Z")),
      stargazers_count = 26544L,
      updated_at =
        ZonedDateTime.from(ISO_DATE_TIME.parse("2020-06-27T15:25:49Z")),
      url = "https://api.github.com/repos/apache/spark",
      watchers = 26544L
    )

    lazy val searchResultsModel = SearchResults(
      incomplete_results = false,
      items = List(apacheRepoModel)
    )
  }
}
