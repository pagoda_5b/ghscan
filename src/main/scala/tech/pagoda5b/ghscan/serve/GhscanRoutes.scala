package tech.pagoda5b.ghscan.serve

import tech.pagoda5b.ghscan.sqlstore.CodeRepositoryStorage
import tech.pagoda5b.ghscan.serve.RepositoryCodecs._
import cats.effect.IO
import org.http4s.HttpRoutes
import org.http4s.dsl.io._

object GhscanRoutes {
  private object SearchTermParam
      extends OptionalQueryParamDecoderMatcher[String]("search")

  /** Provides the server http routes to handle any incoming request */
  def challenge(repoStorage: CodeRepositoryStorage): HttpRoutes[IO] = {
    HttpRoutes.of[IO] {
      case GET -> Root / "challenge" :? SearchTermParam(searchTerm) =>
        val results = searchTerm match {
          case None       => repoStorage.readAll()
          case Some(term) => repoStorage.findAnyContaining(term)
        }
        Ok(results)
    }
  }

}
