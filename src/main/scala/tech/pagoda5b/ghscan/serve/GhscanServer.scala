package tech.pagoda5b.ghscan.serve

import tech.pagoda5b.ghscan.sqlstore.CodeRepositoryStorage
import cats.effect.{IO, ContextShift, Timer}
import fs2.Stream
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger

import scala.concurrent.ExecutionContext.global

object GhscanServer {

  /** Returns the streaming effect represented
    * by an http-server handling multiple requests/response
    * transformations, in the idea of a server as a function.
    *
    * @param storage the sql data-store where repositories are read
    * @param ioShift async context
    * @param ioTimer local clock
    */
  def streamFrom(
      storage: CodeRepositoryStorage
  )(
      implicit
      ioShift: ContextShift[IO],
      ioTimer: Timer[IO]
  ): Stream[IO, Nothing] = {
    // Combine Service Routes into an HttpApp.
    val httpApp = GhscanRoutes.challenge(storage).orNotFound

    // With Logging Middlewares in place
    val loggedHttpApp = Logger.httpApp(true, true)(httpApp)

    BlazeServerBuilder[IO](global)
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(loggedHttpApp)
      .serve
  }.drain
}
