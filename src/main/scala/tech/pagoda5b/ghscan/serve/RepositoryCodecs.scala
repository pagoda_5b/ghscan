package tech.pagoda5b.ghscan.serve

import tech.pagoda5b.ghscan.sqlstore.public.Repository
import cats.effect.IO
import io.circe.{Encoder, Decoder}
import io.circe.generic.semiauto._
import org.http4s.{EntityDecoder, EntityEncoder}
import org.http4s.circe._

/** Import this "module" to be able to send/receive [[Repository]] items
  * as json objects over the wire
  */
object RepositoryCodecs {

  //required codecs based on circe
  implicit val repoDecoder: Decoder[Repository] = deriveDecoder[Repository]

  implicit def repoEntityDecoder: EntityDecoder[IO, Repository] =
    jsonOf[IO, Repository]

  implicit val repoEncoder: Encoder[Repository] = deriveEncoder[Repository]

  implicit def repoEntityEncoder: EntityEncoder[IO, Repository] =
    jsonEncoderOf[IO, Repository]
}
