package tech.pagoda5b.ghscan

import tech.pagoda5b.ghscan.sqlstore.CodeRepositoryStorage
import tech.pagoda5b.ghscan.collect.github.Scanner
import tech.pagoda5b.ghscan.serve.GhscanServer
import tech.pagoda5b.ghscan.config.AppConfig._
import cats.implicits._
import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource}
import doobie._
import doobie.hikari._
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import ciris._

object Main extends IOApp {

  private val apiAuthTokenKey = ConfigKey("GitHub api token")

  /* api token warning*/
  val missingToken =
    """
      | No GitHub Api Token was provided, which could degrade the application capabilities
      | You can pass your token as a command line argument to the application
      | or as an environment variable with key GH_API_TOKEN
      |
      | Plese refer to https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token
      | to learn how to get a token for your GitHub accounts""".stripMargin

  def run(args: List[String]) = {
    implicit val asyncPool =
      ExecutionContexts.cachedThreadPool[IO]

    println(s"args: >>>>>> ${args.mkString(",")}")
    val tokenConfig = readApiToken(args.headOption)

    (prepareTransactor, asyncPool).tupled.use {
      case (tx, asyncContext) =>
        //needed for multiple async operations
        implicit val ac = asyncContext

        //provides access to the data-storage
        val localSqlStorage = CodeRepositoryStorage(tx)

        for {
          logger <- Slf4jLogger.create[IO]
          token <- tokenConfig.load[IO]
          _ <- logger.info(
            "I'm syncing with the online source, collecting repositories data."
          ) *> logger.warn(missingToken).whenA(token.isEmpty)
          scanOutcome <- Scanner.scan(localSqlStorage, token.map(_.value))
          exit <- scanOutcome match {
            case Left(error) =>
              logger
                .error(error)(
                  "Something went wrong as I was trying to collect data."
                )
                .as(ExitCode.Error)
            case Right(_) =>
              logger.info(
                "The data collection completed fine. I'm starting to serve the data locally"
              ) *>
                GhscanServer
                  .streamFrom(localSqlStorage)
                  .compile
                  .drain
                  .as(ExitCode.Success)
          }
        } yield exit
    }
  }

  /* Currently pass hardcoded parameters, which should be read via ciris config
   * from environment or some file
   */
  private def prepareTransactor: Resource[IO, HikariTransactor[IO]] =
    for {
      connectPool <- ExecutionContexts.fixedThreadPool[IO](16)
      blocking <- Blocker[IO]
      transactor <- HikariTransactor.newHikariTransactor[IO](
        driverClassName = "org.postgresql.Driver",
        url = "jdbc:postgresql:scanned-repos",
        user = "ghscanner",
        pass = "ghpass",
        connectEC = connectPool,
        blocker = blocking
      )
    } yield transactor

  /** Reads a possible configuration value for the api token */
  private def readApiToken(
      cliArg: Option[String]
  ): ConfigValue[Option[Secret[AuthApiToken]]] = {
    //try to get the api token for github, first as a command line arg, then as an env-var
    val tokenFromCLI = cliArg match {
      case None              => ConfigValue.missing[String](apiAuthTokenKey)
      case Some(tokenString) => ConfigValue.default(tokenString)
    }

    (tokenFromCLI or env("GH_API_TOKEN")).map(AuthApiToken(_)).secret.option
  }
}
