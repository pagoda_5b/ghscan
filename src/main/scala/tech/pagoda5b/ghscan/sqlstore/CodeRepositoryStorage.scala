package tech.pagoda5b.ghscan.sqlstore

import tech.pagoda5b.ghscan.sqlstore.public.Repository
import java.time._
import scala.concurrent.duration._
import cats.implicits._
import cats.effect.{IO, Timer, ContextShift}
import io.getquill.{idiom => _, _}
import doobie.implicits._
import doobie.quill.DoobieContext
import doobie.util.transactor.Transactor
import fs2.Stream

object CodeRepositoryStorage {

  /** smart constructor */
  def apply(transactor: Transactor[IO])(
      implicit
      ioShift: ContextShift[IO],
      ioTimer: Timer[IO]
  ) = new CodeRepositoryStorage(transactor)

}

/** Provides sql operations to read/write
  * Github repositories entries to/from persistent storage.
  *
  * We avoid creating a proper abstraction for the
  * moment, but this could turn out useful in a
  * second moment, to allow multiple underlying
  * sql engines to be used.
  *
  * Caveat: unfortunately the naming doesn't help.
  * Don't get confused, here we call "storage" the
  * persistecy abstraction, commonly called Repository or DAO
  * in DDD-parlance.
  * That's because the modelled object is itself a (code) repository,
  * that is, a git repository of shared code on github.
  *
  * To create the instance we need a constructor dependency on
  * a Doobie [[Transactor]], which is fine since we chose to
  * avoid abstraction, as stated above.
  */
class CodeRepositoryStorage(
    transactor: Transactor[IO]
)(
    implicit
    ioShift: ContextShift[IO],
    ioTimer: Timer[IO]
) {
  /* Design Notes
   * We choose a no-abtration approach here, and assume directly that we deal
   * with a concrete IO type from cats-effect.
   *
   * Here error handling is not yet explicitly defined in the return
   * types. The IO wrapper is currently the carrier type for failures.
   * Under consideration: use Either as an explicit alternative.
   */

  /* We mix usage of Quill as a query language dsl and Doobie as
   * engine and transactor.
   * For additional context on the integration, refer to
   * https://tpolecat.github.io/doobie/docs/17-Quill.html
   *
   * This context is used to define queries and give access to the
   * proper sql dsl.
   */
  private val ctx = new DoobieContext.Postgres(SnakeCase)
  import ctx._

  /** Matches an existing [[Repository]] on its identifier
    *
    * @param searchId the unique identifier
    */
  def findById(searchId: Long): IO[Option[Repository]] = {
    val find = quote {
      query[Repository].filter(_.id == lift(searchId))
    }
    run(find)
      .map(_.headOption)
      .transact(transactor)
  }

  /** Returns [[Repository]] entries that match the argument
    * on any of its `full name` or `description`.
    *
    * @param textToMatch a non-empty text to be matched
    * @return a possibly empty stream of results
    */
  def findAnyContaining(textToMatch: String): Stream[IO, Repository] = {
    val textLike = s"%$textToMatch%"
    val find = quote {
      query[Repository].filter(repo =>
        repo.fullName.like(lift(textLike)) ||
          repo.description.exists(_.like(lift(textLike)))
      )
    }
    Stream.evalSeq(
      run(find).transact(transactor)
    )
  }

  /** Returns any stored [[Repository]] entry to the caller, as a stream. */
  def readAll(): Stream[IO, Repository] = {
    //currently hand-limiting the results, will be done via pagination later
    val read = quote {
      query[Repository]
        .sortBy(_.updatedAt)(Ord.descNullsLast)
        .take(100)
    }
    Stream.evalSeq(
      run(read).transact(transactor)
    )
  }

  /** Seek the most recent push date for any repository available on storage. */
  def findLatestPush(): IO[Option[ZonedDateTime]] = {
    val find = quote {
      query[Repository].map(_.pushedAt).max
    }
    run(find)
      .map(_.map(_.atZone(ZoneOffset.UTC)))
      .transact(transactor)
  }

  /** Will attempt to write the entity to the underlying
    * storage, returning the number representing added rows.
    *
    * The unique id of the repository will be considered to
    * decide if an existing entry should be updated or a new
    * record will be added.
    *
    * @param repository to store
    */
  def writeOne(repository: Repository): IO[Long] = {
    val insertOrUpdate = quote {
      query[Repository]
        .insert(liftCaseClass(repository))
        .onConflictUpdate(_.id)(
          (existing, proposed) => existing.createdAt -> proposed.createdAt,
          (existing, proposed) =>
            existing.defaultBranch -> proposed.defaultBranch,
          (existing, proposed) => existing.description -> proposed.description,
          (existing, proposed) => existing.forks -> proposed.forks,
          (existing, proposed) => existing.fullName -> proposed.fullName,
          (existing, proposed) => existing.gitUrl -> proposed.gitUrl,
          (existing, proposed) => existing.homepage -> proposed.homepage,
          (existing, proposed) => existing.name -> proposed.name,
          (existing, proposed) => existing.openIssues -> proposed.openIssues,
          (existing, proposed) => existing.pushedAt -> proposed.pushedAt,
          (existing, proposed) =>
            existing.stargazersCount -> proposed.stargazersCount,
          (existing, proposed) => existing.updatedAt -> proposed.updatedAt,
          (existing, proposed) => existing.url -> proposed.url,
          (existing, proposed) => existing.watchers -> proposed.watchers
        )
    }
    run(insertOrUpdate).transact(transactor)
  }

  /** Will perform the same operation as [[writeOne]], for all
    * of the argument stream elements, returning the number of
    * added rows.
    *
    * Same consideration on update or insert applies.
    *
    * @param repositories to store
    */
  def writeMany(repositories: Stream[IO, Repository]): Stream[IO, Long] =
    repositories
      .groupWithin(50, 2.seconds) //we're gonna batch the operations
      .evalMapChunk { chunk =>
        val inserts = quote {
          liftQueryCaseClass[List, Repository](chunk.toList).foreach {
            repository =>
              query[Repository]
                .insert(repository)
                .onConflictUpdate(_.id)(
                  (existing, proposed) =>
                    existing.createdAt -> proposed.createdAt,
                  (existing, proposed) =>
                    existing.defaultBranch -> proposed.defaultBranch,
                  (existing, proposed) =>
                    existing.description -> proposed.description,
                  (existing, proposed) => existing.forks -> proposed.forks,
                  (existing, proposed) => existing.fullName -> proposed.fullName,
                  (existing, proposed) => existing.gitUrl -> proposed.gitUrl,
                  (existing, proposed) => existing.homepage -> proposed.homepage,
                  (existing, proposed) => existing.name -> proposed.name,
                  (existing, proposed) =>
                    existing.openIssues -> proposed.openIssues,
                  (existing, proposed) => existing.pushedAt -> proposed.pushedAt,
                  (existing, proposed) =>
                    existing.stargazersCount -> proposed.stargazersCount,
                  (existing, proposed) =>
                    existing.updatedAt -> proposed.updatedAt,
                  (existing, proposed) => existing.url -> proposed.url,
                  (existing, proposed) => existing.watchers -> proposed.watchers
                )
          }
        }
        run(inserts).transact(transactor).map(_.sum)
      }

}
