package tech.pagoda5b.ghscan.sqlstore.public

case class Repository(
    id: Long,
    name: String,
    fullName: String,
    description: Option[String],
    url: String,
    gitUrl: String,
    homepage: Option[String],
    stargazersCount: Option[Long],
    watchers: Option[Long],
    createdAt: java.time.LocalDateTime,
    updatedAt: java.time.LocalDateTime,
    pushedAt: java.time.LocalDateTime,
    defaultBranch: String,
    forks: Option[Long],
    openIssues: Option[Int]
)
