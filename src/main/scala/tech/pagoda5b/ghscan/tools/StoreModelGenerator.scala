package tech.pagoda5b.ghscan.tools

import scala.concurrent.Future
import cats.implicits._
import cats.effect.{IOApp, ExitCode, IO}
import io.getquill.codegen.jdbc.SimpleJdbcCodegen
import io.getquill.codegen.model.SnakeCaseNames
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger

import scala.concurrent.ExecutionContext.Implicits.global
object StoreModelGenerator extends IOApp {
  val gen = new SimpleJdbcCodegen("postgres", "tech.pagoda5b.ghscan.sqlstore") {
    override def nameParser = SnakeCaseNames
  }

  override def run(args: List[String]): IO[ExitCode] =
    for {
      logger <- Slf4jLogger.create[IO]
      paths <- IO.fromFuture(
        IO(
          Future.sequence(
            gen.writeFiles("src/main/scala/tech/pagoda5b/ghscan/sqlstore")
          )
        )
      )
      _ <- logger.info(
        s"""The following files have been generated: ${paths.mkString("\n")}"""
      )
    } yield ExitCode.Success
}
