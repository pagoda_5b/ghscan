package tech.pagoda5b.ghscan.collect.sql

import tech.pagoda5b.ghscan.collect.CollectStorage
import tech.pagoda5b.ghscan.sqlstore.CodeRepositoryStorage
import tech.pagoda5b.ghscan.sqlstore.public.Repository
import cats.effect.IO
import fs2.Stream

object SqlCollectStorage {

  /** smart constructor */
  def apply(sqlStorage: CodeRepositoryStorage) =
    new SqlCollectStorage(sqlStorage)

}

/** Implements a data collection storage using a sql storage,
  * for [[Repository]] entries.
  */
class SqlCollectStorage(
    sqlStorage: CodeRepositoryStorage
) extends CollectStorage[Repository, Long] {
  /* Streaming the data is quite easy, having no impedance
   * mismatch on the underlying storage types
   */

  override def storeCollected(
      data: Stream[IO, Repository]
  ): Stream[IO, Long] =
    sqlStorage.writeMany(data)

}
