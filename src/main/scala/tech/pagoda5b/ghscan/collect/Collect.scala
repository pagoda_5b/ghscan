package tech.pagoda5b.ghscan.collect

import fs2._
import cats.effect.IO

/** Defines an abstract composition that is expected
  * to read data from an external source and collect
  * it to a storage of sort.
  */
object CollectionProcess {

  /** Combines source and store streams to "pipeline" data from the former to the latter.
    * We use a streaming semantic, and assume the data comes in a different format from
    * what is expected from storage, so that an [implicit] adapter is used in-between.
    * All operations produce [[IO]] effects, and the adaptation should conform to the
    * same effect.
    *
    * @param source will provide a stream of data
    * @param store will collect a stream of data
    * @param lastCollected will possibly signal a "watermark" level to start collecting data
    * @param connector to bridge the data
    * @tparam SourceElement the type of incoming data, as defined by `source`
    * @tparam StoreElement the type of data to store, as defined by `store`
    * @tparam Mark optionally limits the collected data, as a type to pass to the `source`
    * @tparam Stored the type of results from running the `store` operations
    * @return an effect that will actually execute the whole operation, with explicitly
    *         a failing results, in case of error
    */
  def run[SourceElement, StoreElement, Mark, Stored](
      source: CollectSource[SourceElement] { type LastCollected = Mark },
      store: CollectStorage[StoreElement, Stored],
      collectAfter: Option[Mark] = None
  )(
      implicit connector: DataAdapter[IO, SourceElement, StoreElement]
  ): IO[Either[Throwable, Unit]] = {
    /* We want to build a single streaming pipeline that
     * will connect source and storage, adapting between
     * formats.
     * For the latter, we also convert the connector result
     * into a an IO operation, compatible with the streaming
     * effect.
     * We're ignoring the results of storing the data, but
     * this could be eventually improved upon if metrics should
     * ever be needed, probably asking for additional constraints
     * on the `Stored` type (e.g. should have a Monoid)
     */

    source
      .collectFrom(collectAfter)
      .flatMap(connector.adapt)
      .through(store.storeCollected)
      .compile
      .drain
      .attempt
  }

}

/** Generic source (incoming data) that
  * describe how to read data from an external
  * system input.
  */
trait CollectSource[Data] {

  /** A marker type to identify the latest collection execution */
  type LastCollected

  /** Retrieves repository data from
    * the source.
    * Data is collected as an effectful [[fs2.Stream]]
    * of values.
    *
    * @param lastCollected optionally signals if the collection process previously
    *                      stored data up to a specific point, as to avoid retrieving
    *                      what was already collected
    * @return the stream
    */
  def collectFrom(
      lastCollected: Option[LastCollected]
  ): Stream[IO, Data]

}

/** Generic sink where collected data can be sent
  * to be permanently stored
  *
  * The [[Stored]] type Generically describes the result of each storage operation
  */
trait CollectStorage[Data, Stored] {

  /** Actually writes the data to an underlying storage
    * system, usually a database.
    *
    * @param data to be written, as an effectful stream
    */
  def storeCollected(data: Stream[IO, Data]): Stream[IO, Stored]
}

/** One-way conversion of data format, which might
  * come useful to bridge the data collector
  * with the storage.
  *
  * In practice, it simplifies the [[CollectSource]]
  * and [[CollectStorage]] job, by taking care of
  * translating between the most natural representations
  * for each system.
  *
  * We have to assume that more than one element can
  * result for each input, and handle that with a streaming
  * interface as a return value.
  */
trait DataAdapter[F[_], DataIn, DataOut] {

  /** Converts input format to
    * output format, with the assumption
    * that the underlying data should preserve
    * its meaning.
    *
    * @param from what we have
    * @return what we need, or an error
    */
  def adapt(from: DataIn): Stream[F, DataOut]
}
