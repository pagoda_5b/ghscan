package tech.pagoda5b.ghscan.collect.github

import tech.pagoda5b.ghscan.collect._
import RepositoriesModel._
import tech.pagoda5b.ghscan.sqlstore.public.Repository
import tech.pagoda5b.ghscan.sqlstore.CodeRepositoryStorage
import tech.pagoda5b.ghscan.collect.sql.SqlCollectStorage
import java.time._
import java.time.format.DateTimeFormatter
import scala.concurrent.ExecutionContext
import org.http4s.client.blaze.BlazeClientBuilder
import fs2.Stream
import cats.implicits._
import cats.effect.IO
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import tech.pagoda5b.ghscan.config.AppConfig.AuthApiToken

/** Connects to github and downloads all data available
  * for repositories, converting them and storing them
  * in a local format, apt for later exposing it via
  * some corresponding server process.
  */
object Scanner {

  /* Brings the model adapter into scope */
  import ModelAdapter._

  /** Returns the effectful program that collects the data.
    * The returned value explicitly provides failures in the [[Either]] type
    * following common conventions.
    *
    * @param dbTransactor needed to access the sql storage
    * @param asyncContext runs concurrent operations
    */
  def scan(repoStorage: CodeRepositoryStorage, authToken: Option[AuthApiToken])(
      implicit asyncContext: ExecutionContext
  ): IO[Either[Throwable, Unit]] = {
    implicit val ioShift = IO.contextShift(asyncContext)

    BlazeClientBuilder[IO](asyncContext).resource.use { client =>
      for {
        logger <- Slf4jLogger.create[IO]
        _ <- logger.info("I'm checking for locally stored data")
        latestMark <- repoStorage.findLatestPush()
        _ <- latestMark.isDefined
          .pure[IO]
          .ifM(
            ifTrue = logger.info(
              s"There is data available, I will collect anything updated after ${showMark(latestMark)}"
            ),
            ifFalse = logger.info(
              "Nothing available, I'm getting something from the source"
            )
          )
        outcome <- CollectionProcess.run(
          source = ScalaRepositoriesSource(client, authToken),
          store = SqlCollectStorage(sqlStorage = repoStorage),
          collectAfter = latestMark
        )
      } yield outcome
    }
  }

  /** simple utility to print the last date mark available */
  private val showMark: Option[ZonedDateTime] => String = {
    case None      => "some unspecified point in time"
    case Some(zdt) => zdt.format(DateTimeFormatter.ISO_DATE_TIME)
  }

}

/** Provides the adapter to bridge the model representations */
object ModelAdapter {

  /* Maps corresponding fields from the input rest model to the output sql model */
  private val conversion = (from: RepositoryItem) =>
    Repository(
      id = from.id,
      name = from.name,
      fullName = from.full_name,
      description = from.description,
      url = from.url,
      gitUrl = from.git_url,
      homepage = from.homepage,
      stargazersCount = from.stargazers_count.some,
      watchers = from.watchers.some,
      createdAt = from.created_at.toLocalDateTime(),
      updatedAt = from.updated_at.toLocalDateTime(),
      pushedAt = from.pushed_at.toLocalDateTime(),
      defaultBranch = from.default_branch,
      forks = from.forks.some,
      openIssues = from.open_issues.some
    )

  /** An adapter that turns the api results into a sql model */
  implicit val dataAdapter: DataAdapter[IO, SearchResults, Repository] =
    (sr: SearchResults) => {
      Stream.fromIterator[IO](sr.items.iterator).map(conversion)
    }

}
