package tech.pagoda5b.ghscan.collect.github

import java.time.ZonedDateTime
import io.circe.generic.semiauto._
import io.circe.Codec
import cats.Show

/** Expose models for the repositories objects, as returned by the
  * github api we're using.
  */
object RepositoriesModel {

  /** This is the main wrapper for
    * the search results
    *
    * see also https://developer.github.com/v3/search/#timeouts-and-incomplete-results
    *
    * @param incomplete_results if `True` the response timed-out and the results are not complete
    * @param items the actual result entries
    */
  case class SearchResults(
      incomplete_results: Boolean,
      items: List[RepositoryItem]
  )

  /** A repository definition as returned by the search query */
  case class RepositoryItem(
      created_at: ZonedDateTime,
      default_branch: String,
      description: Option[String],
      forks: Long,
      full_name: String,
      git_url: String,
      homepage: Option[String],
      id: Long,
      name: String,
      open_issues: Int,
      pushed_at: ZonedDateTime,
      stargazers_count: Long,
      updated_at: ZonedDateTime,
      url: String,
      watchers: Long
  )

  implicit val RepositoryCodec: Codec[RepositoryItem] =
    deriveCodec[RepositoryItem]

  implicit val SearchResultsCodec: Codec[SearchResults] =
    deriveCodec[SearchResults]

  implicit val ShowRepositoryItem = Show.fromToString[RepositoryItem]
  implicit val ShowSearchResults = Show.fromToString[SearchResults]

}
