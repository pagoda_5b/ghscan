package tech.pagoda5b.ghscan.collect.github

import tech.pagoda5b.ghscan.collect.CollectSource
import tech.pagoda5b.ghscan.config.AppConfig.AuthApiToken
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import scala.util.control.NoStackTrace
import org.http4s._
import org.http4s.implicits._
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.headers._
import org.http4s.Method.GET
import org.http4s.client.Client
import org.http4s.client.dsl.io._
import org.http4s.client.middleware.FollowRedirect
import cats.implicits._
import cats.effect.{IO, ContextShift}
import fs2._

/** This is a data collector
  * that scans github api endpoints to fetch
  * entries on github repositories.
  *
  * It's meant to be used within a [[CollectingProcess]]
  *
  * The performance of the source collector will be improved if
  * an api token is provided as an extra parameter.
  * For details on how to get a valid token see [[AuthApiToken]]
  *
  * @param restClient dependency used to access the rest apis
  * @param authToken the github-api token used to reduce rate-limiting of
  *                   the requests/minute and to access permission-based data
  */
class ScalaRepositoriesSource(
    restClient: Client[IO],
    authToken: Option[AuthApiToken] = None
)(implicit shift: ContextShift[IO])
    extends CollectSource[RepositoriesModel.SearchResults] {
  import ScalaRepositoriesSource._
  import RepositoriesModel._

  //TODO we have to pass extra configuration from the outside to
  // correctly build GH api calls with query params
  // (per_page results: max 100 for a search endpoint)

  type LastCollected = ZonedDateTime

  /* These are the expected headers to use when calling the remote api */
  private val callHeaders =
    Accept(MediaType.application.json) ::
      `User-Agent`(AgentProduct("ghscan-app")) ::
      authToken.map(toAuthHeader).toList

  /** Enables Http redirects, as per github api spec */
  private val client =
    FollowRedirect(maxRedirects = 1)(restClient)

  override def collectFrom(
      lastCollected: Option[ZonedDateTime] = None
  ): Stream[IO, SearchResults] = {
    val result = client
      .expect[SearchResults](
        GET(Endpoints.search(updatedAfter = lastCollected), callHeaders: _*)
      )
      .adaptErr {
        case InvalidMessageBodyFailure(details, cause) =>
          ProtocolError(s"Body invalid: $details", cause)
        case MalformedMessageBodyFailure(details, cause) =>
          ProtocolError(s"Body malformed: $details", cause)
        case InvalidResponseException(msg) =>
          ProtocolError(s"Response invalid: $msg")
        case MediaTypeMismatch(messageType, expected) =>
          ProtocolError(
            s"Incorrect media-type: ${messageType.mainType} . Expecting: ${expected.show}"
          )
        case MediaTypeMissing(expected) =>
          ProtocolError(s"Missing media-type: ${expected.show}")
        case ParseFailure(sanitized, details) =>
          ProtocolError(s"Failed to parse the results: $sanitized")
      }

    /* We can further retrieve multiple results using
     * metering of the a stream built by repeatedly calling
     * with different page numbers, up to a value that will be determined
     * from the response headers (e.g. we can peek at the last page num)
     * Caveat: we always risk missing some of the most updated entries or having
     * duplicates if the repositories gets updated during the stream execution.
     * This could be further sophisticated by limiting the next queries
     * using information from the first page of results but sounds like
     * a lot of work to do it right.
     */
    Stream.eval(result)
  }

}

/** This source will reach to github api v3 and send a
  * search request for scala repositories, returning
  * values sorted by latest updated.
  */
object ScalaRepositoriesSource {

  /** smart constructor */
  def apply(
      restClient: Client[IO],
      authToken: Option[AuthApiToken] = None
  )(implicit shift: ContextShift[IO]): ScalaRepositoriesSource =
    new ScalaRepositoriesSource(restClient, authToken)

  /** wraps the token in a HTTP Header for authorization */
  private val toAuthHeader: AuthApiToken => Header = apiToken =>
    Authorization(BasicCredentials(apiToken.token))

  /* scopes github web endpoints information */
  private object Endpoints {

    /** Search api for repositories
      *
      * @param page used to get extra paginated results from the endpoints, must be a positive Int
      * @param updatedAsOf limits responses to only those repositories updated after this value
      */
    def search(
        page: Option[Int] = None,
        updatedAfter: Option[ZonedDateTime] = None
    ) =
      baseUri
        .withQueryParam(
          "q",
          "language:scala mirror:false " + pushedParam(updatedAfter)
        )
        .withQueryParam("page", page.getOrElse(1).show)

    //base REST endpoint
    private[this] val repositories =
      uri"https://api.github.com/search/repositories"

    /* compose the parameter for the query to get only repos being pushed at
     * after a given date-time
     * see https://help.github.com/en/github/searching-for-information-on-github/searching-for-repositories#search-by-when-a-repository-was-created-or-last-updated
     */
    private[this] val pushedParam: Option[ZonedDateTime] => String = {
      case None => ""
      case Some(time) =>
        s"pushed:>${time.format(DateTimeFormatter.ISO_DATE_TIME)}"
    }

    //URI built with the fixed params
    val baseUri =
      repositories
        .withQueryParam("per_page", "50") //needs be configured
        .withQueryParam("sort", "updated")
        .withQueryParam("order", "desc")

  }

  case class ProtocolError[T <: Throwable](
      message: String,
      cause: Option[T] = None
  ) extends RuntimeException
      with NoStackTrace

}
