package tech.pagoda5b.ghscan.config

import cats.Show

/** Collects configuration values */
object AppConfig {

  /** The token can be used to authorize a higher rate limit
    * to call the github apis, and to return results based
    * on the user permissions given to the token
    *
    * see https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token
    * @param token a string
    */
  case class AuthApiToken(token: String) extends AnyVal

  //used to print as a config
  implicit val showAuthToken = Show.show[AuthApiToken](_.token)
}
