create table public.Repository (
      id bigint primary key,
      name character varying not null,
      full_name character varying not null,
      description character varying,
      url character varying not null,
      git_url character varying not null,
      homepage character varying,
      stargazers_count bigint,
      watchers bigint,
      created_at timestamp with time zone not null,
      updated_at timestamp with time zone not null,
      pushed_at timestamp with time zone not null,
      default_branch character varying not null,
      forks bigint,
      open_issues integer
)