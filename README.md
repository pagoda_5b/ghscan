# GH-Scan #

Welcome to "Collect And Serve" the service to read data from GH and expose it for your perusal in a
locally available database, with a http server api in front.

In particular we collect the most recently updated projects (repositories) made with Scala

Sounds great, indeed!

## Overview

The Application here provided will run two services/processes which will

 1. connect with the GH api interface to download the latest data
 2. run an http server to show data stored therein

It abuses a lot of purely functional or FP-oriented libraries, in particular

 * cats
 * cats-effects
 * fs2
 * http4s
 * circe
 * ciris
 * log4cats
 * doobie
 * quill

## How to run it

**Start the db**

To run correctly, a database should be available to store the data.
You can run a docker container with Postgres 12.x just by calling `docker-compose up` in the project dir.

**To run the application you can**

 * Use sbt : `sbt "run [YOUR_API_TOKEN]"` and select the `tech.pagoda5b.ghscan.Main` class to run (or provide it to sbt directly with run-main)
 * Use bloop : `bloop run ghscan -m tech.pagoda5b.ghscan.Main`, to which I don't know how to pass the Token argument, honestly
 * Assemble a fat-jar : `sbt assembly` that will produce a `uberjar/ghscan.jar` file in your project directory which you can run as a script

Another way to provide the GitHub Api Token is to set an env-var named `GH_API_TOKEN` and run with no arguments.

**Checking the data**

Once the server is up and running, as the log messages should tell you, the data can be inspected via the following endpoint

`http://localhost:8080/challenge`

This will present you with a chunked streaming response of json objects representing the key info for each repository.

The results will be sorted with the most recently updated results first.

You can additionally search for a specific repository using a search term as a query parameter, as in

`http://localhost:8080/challenge?search=black-lives-matter`

Which sadly won't probably give you any result

## How to test it

Unfortunately the test code at this moment fails to compile with a quite exotic compiler error, which might warrant a proper bug submission.

As you can see for yourself if you try to `sbt test` the project, you'll be presented with an excellent example of esoteric error message.

Honestly I'm not sure I'll be able to addres  the situation properly in the near future.

### Compilation note
The build is based on JVM-11

If by any chance you can't have that version on your machine but want to build the project nonetheless, change the
`build.sbt` file setting overriding the option "-target" to `8`, which is commonly available on any scala-dev machine.

## Still missing

 * The collector only gets the first page of github results, it should be improved to loop until the rate limit or
    some self-defined number of results is hit. This requires parsing the Headers to get page-numbers and all the fuss.
 * The rate limiting shold be easy to address considered fs2 metering functions, which could allow to collect data
    without stopping, even as the server starts to make the data available
 * The results from our own server are streamed but not paginated. Right now a limited number of results are shown, but
    it should provide links in the headers to navigate the pages, using fs2 streams to select the page elements
 * The configuration is bare bone and mostly hardcoded (e.g. database credentials), yet I don't think it would be
    worth the pain as a priority in this kind of project
 * I'd like to have more tests, which are quite scarce I admit - given the compilation issue too - but it would first
    require some work to abstract some code "modules" to be more easily replaceable with dummies/fakes, like the one that
    read from db

---
## License ##

This code is open source software licensed under the
[MIT](https://opensource.org/licenses/MIT) license.
